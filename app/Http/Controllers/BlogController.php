<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Blog;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function index(){

        $blogs = Blog::with('comments')->get();
        return Inertia::render('Blog', ['data' => $blogs]);
    }

    public function store(Request $request){
        Blog::create($request->all());
        return redirect()->back();
    }

    public function show($id){
        $blog = Blog::find($id);

        return Inertia::render('Auth/Blog/Edit', ['data' => $blog]);
    }
    public function update(Request $request, $id){
        $blog = Blog::find($id);

        $blog->update($request->all());

        return redirect()->back();
    }

    public function delete($id){
        $blog = Blog::find($id);

        $blog->delete();

        return redirect()->back();
    }
}
