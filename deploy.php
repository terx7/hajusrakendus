<?php

namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'namesong');
set('remote_user', 'virt83024'); //virt...
set('http_user', 'virt83024');
set('keep_releases', 2);

// Hosts
host('tak19leemet.itmajakas.ee')
    ->setHostname('tak19leemet.itmajakas.ee')
    ->set('http_user', 'virt83024')
    ->set('deploy_path', '~/domeenid/www.tak19leemet.itmajakas.ee/hajus')
    ->set('branch', 'master');

// Tasks
set('repository', 'git@gitlab.com:terx7/hajusrakendus.git');
//Restart opcache
task('opcache:clear', function () {
    run('killall php80-cgi || true');
})->desc('Clear opcache');

task('build:node', function () {
    cd('{{release_path}}');
    run('npm i');
    run('npx vite build');
    run('rm -rf node_modules');
});
task('deploy', [
    'deploy:prepare',
    'deploy:vendors',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'build:node',
    'deploy:publish',
    'opcache:clear',
    'artisan:cache:clear'
]);
after('deploy:failed', 'deploy:unlock');
