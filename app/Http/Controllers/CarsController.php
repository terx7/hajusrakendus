<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use Inertia\Inertia;

class CarsController extends Controller
{
    public function index(){
        $cars = Cars::all();

        return $cars;
    }

    public function addCar(){
        return Inertia::render("Cars");
    }

    public function store(Request $request){
        $data = $request->all();
        if($request->file('image')){
            $file = $request->file('image');
            $filename = date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('Image'), $filename);
            $data['image'] = 'images/'.$filename;
        }
        return $data;
        $cars = Cars::create($data);

        return $cars;
    }
}
