<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\WeatherController;
use App\Http\Controllers\MarkerController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/blog', [BlogController::class, 'index'])->name('blog');
Route::delete('/blog/{id}',[BlogController::class, 'delete'])->name('delete.blog');
Route::get('/blog/{id}', [BlogController::class, 'show'])->name('edit.blog');
Route::put('/blog/{id}', [BlogController::class, 'update'])->name('update.blog');
Route::post('/blog/add', [BlogController::class, 'store'])->name('add.blog');

Route::get('/weather', [WeatherController::class, 'weather'])->name('weather');

Route::get('/map', [MarkerController::class, 'index'])->name("map");
Route::post('/map', [MarkerController::class, 'store'])->name('add.marker');
Route::post('/map/marker/{id}', [MarkerController::class, 'update'])->name("update.marker");
Route::get('/map/marker/{id}', [MarkerController::class, 'show'])->name('edit.marker');
Route::delete('/map/{id}', [MarkerController::class, 'delete'])->name('delete.marker');

Route::get('/', [StoreController::class, 'index'])->name('store');
Route::post('/cart', [StoreController::class, 'cartAdd'])->name('add.cart');
Route::get('/cart', [StoreController::class, 'cartList'])->name('cart.list');
Route::put('/cart/{id}', [StoreController::class, 'cartUpdate'])->name('update.cart');
Route::delete('/cart/{id}', [StoreController::class, 'destroy'])->name('delete.cart');

Route::get('/stripeCart',[StoreController::class, 'checkoutData'])->name('get.stripeCart');

Route::get('/success', [StoreController::class, 'success']);

Route::post("/comment/{blog}", [CommentController::class, 'store']);
Route::delete("/comment/{id}", [CommentController::class, 'delete'])->name("delete.comment");

require __DIR__.'/auth.php';
